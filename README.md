# Foo Challenge

## Build

```
cd foo-service
./gradlew bootJar
mkdir -p build/dependency
cd build/dependency
jar -xf ../libs/*.jar
```

## Run

```
docker compose up -d
```
