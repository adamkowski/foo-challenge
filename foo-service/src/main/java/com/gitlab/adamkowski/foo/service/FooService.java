package com.gitlab.adamkowski.foo.service;

import com.gitlab.adamkowski.foo.model.Foo;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Service
public class FooService {
    private static final int DEFAULT_VALUE_LENGTH_LIMIT = 10;
    private static final AtomicLong GET_FOO_COUNTER = new AtomicLong(0L);

    public Foo getFoo() {
        return getFoo(DEFAULT_VALUE_LENGTH_LIMIT);
    }

    public Foo getFoo(int length) {
        return new Foo(getValue(length));
    }

    public Long getFooCounter() {
        return GET_FOO_COUNTER.incrementAndGet();
    }

    private String getValue(int length) {
        return RandomStringUtils.randomAlphabetic(length);
    }
}
