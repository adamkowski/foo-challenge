package com.gitlab.adamkowski.foo.controller;

import com.gitlab.adamkowski.foo.model.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
public class UserController {
    private static final String LOGIN = "TEST_LOGIN";

    @GetMapping("/user")
    public User getUser() {
        return new User(LOGIN);
    }
}
