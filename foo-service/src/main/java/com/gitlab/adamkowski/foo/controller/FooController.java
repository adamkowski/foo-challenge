package com.gitlab.adamkowski.foo.controller;

import com.gitlab.adamkowski.foo.model.Foo;
import com.gitlab.adamkowski.foo.service.FooService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
public class FooController {
    private final FooService fooService;

    public FooController(FooService fooService) {
        this.fooService = fooService;
    }

    @GetMapping("/foo")
    public Foo getFoo() {
        return fooService.getFoo();
    }

    @GetMapping("/foo/{length}")
    public Foo getFoo(@PathVariable int length) {
        return fooService.getFoo(length);
    }

    @GetMapping("/foo-counter")
    public Long getFooCounter() {
        return fooService.getFooCounter();
    }
}
