package com.gitlab.adamkowski.foo.model;

public record User(String login) {
}
