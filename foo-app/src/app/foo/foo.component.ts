import {Component, OnInit} from '@angular/core';

import { map, mergeMap, Observable, tap } from 'rxjs';

import {Select, Store} from '@ngxs/store';

import {FooService} from '../foo.service';
import {FooState} from '../shared/foo.state';
import { SetFooCounter, SetFooValue } from '../shared/app.actions';

@Component({
  selector: 'app-foo',
  templateUrl: './foo.component.html',
  styleUrls: ['./foo.component.css']
})
export class FooComponent implements OnInit {
  @Select(FooState.fooValue) fooValue$: Observable<string>;
  @Select(FooState.fooCounter) fooCounter$: Observable<number>;

  length: number = 1;

  constructor(private store: Store,
              private fooService: FooService) {
  }

  ngOnInit(): void {
  }

  loadFoo(): void {
    this.obtainFooValue()
      .pipe(
        mergeMap(foo => {
          console.log(`Returned foo length: ${foo.payload.length}`);
          return this.obtainFooCounter();
        })
      )
      .subscribe();
  }

  private obtainFooValue(): Observable<SetFooValue> {
    return this.fooService.getFoo(this.length)
      .pipe(
        map(foo => foo.value),
        map(val => new SetFooValue(val)),
        tap(setFooValue => this.store.dispatch(setFooValue)),
      );
  }

  private obtainFooCounter(): Observable<SetFooCounter> {
    return this.fooService.getCounter()
      .pipe(
        map(val => new SetFooCounter(val)),
        tap(setFooCounter => this.store.dispatch(setFooCounter)),
      );
  }
}
