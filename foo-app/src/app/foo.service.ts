import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {Foo} from './foo';

@Injectable({
  providedIn: 'root'
})
export class FooService {
  private fooUrl = 'http://localhost:8080/foo';

  constructor(private http: HttpClient) {
  }

  getFoo(length: number): Observable<Foo> {
    return this.http.get<Foo>(`${this.fooUrl}/${length}`);
  }

  getCounter(): Observable<number> {
    return this.http.get<number>(`${this.fooUrl}-counter`);
  }
}
