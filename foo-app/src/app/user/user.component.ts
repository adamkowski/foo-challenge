import {Component, OnInit} from '@angular/core';

import {map, Observable, tap} from 'rxjs';

import {Select, Store} from '@ngxs/store';

import {UserState} from '../shared/user.state';
import {UserService} from '../user.service';
import {SetUserLogin} from '../shared/app.actions';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Select(UserState.userLogin) userLogin$: Observable<string>;

  constructor(private store: Store,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.getLogin();
  }

  getLogin(): void {
    this.userService.getUser()
      .pipe(
        map(user => user.login),
        map(login => new SetUserLogin(login)),
        tap(setUserLogin => this.store.dispatch(setUserLogin)),
      )
      .subscribe();
  }
}
