import {Injectable} from '@angular/core';

import {Action, Selector, State, StateContext} from '@ngxs/store';

import {SetUserLogin} from './app.actions';

export interface UserStateModel {
  login: string;
}

@State<UserStateModel>({
  name: 'user',
  defaults: {
    login: '',
  },
})
@Injectable({providedIn: "root"})
export class UserState {

  @Selector()
  static userLogin(state: UserStateModel): string {
    return state.login;
  }

  @Action(SetUserLogin)
  setUserLogin(context: StateContext<UserStateModel>, {payload}: SetUserLogin) {
    context.patchState({login: payload})
  }
}
