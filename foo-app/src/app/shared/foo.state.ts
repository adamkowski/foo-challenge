import {Injectable} from '@angular/core';

import {Action, Selector, State, StateContext} from '@ngxs/store';

import { SetFooCounter, SetFooValue } from './app.actions';

export interface FooStateModel {
  value: string;
  counter: number;
}

@State<FooStateModel>({
  name: 'foo',
  defaults: {
    value: '',
    counter: 0,
  },
})
@Injectable({providedIn: "root"})
export class FooState {

  @Selector()
  static fooValue(state: FooStateModel): string {
    return state.value;
  }

  @Selector()
  static fooCounter(state: FooStateModel): number {
    return state.counter;
  }

  @Action(SetFooValue)
  setFooValue(context: StateContext<FooStateModel>, {payload}: SetFooValue) {
    context.patchState({value: payload})
  }

  @Action(SetFooCounter)
  setFooCounter(context: StateContext<FooStateModel>, {payload}: SetFooCounter) {
    context.patchState({counter: payload})
  }
}
