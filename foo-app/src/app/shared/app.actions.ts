export class SetUserLogin {
  static readonly type = '[user] set user login';

  constructor(public payload: string) {
  }
}

export class SetFooValue {
  static readonly type = '[foo] set foo value';

  constructor(public payload: string) {
  }
}

export class SetFooCounter {
  static readonly type = '[foo] set foo counter';

  constructor(public payload: number) {
  }
}
